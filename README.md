
<!--
!   
!     ______      _      _      ____  _____
!    |  ____/\   | |    | |    |___ \|  __ \
!    | |__ /  \  | |    | |      __) | |  | |
!    |  __/ /\ \ | |    | |     |__ <| |  | |
!    | | / ____ \| |____| |____ ___) | |__| |
!    |_|/_/    \_\______|______|____/|_____/
!   
!   
-->

# FALL3D model

<!-- Badges / shields -->
[![SQAaaS badge shields.io](https://img.shields.io/badge/sqaaas%20software-gold-yellow)](https://api.eu.badgr.io/public/assertions/_ZFvOIJRTLKH10aAxXS2zA "SQAaaS gold badge achieved")
<!---
[![GitLab Release (latest by SemVer)](https://img.shields.io/gitlab/v/release/fall3d-suite/fall3d)](https://gitlab.com/fall3d-suite/fall3d/-/releases)
--->
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6343786.svg)](https://doi.org/10.5281/zenodo.6343786)

<img src="Manual/images/logo.png" width="250">

## Overview
FALL3D is a 3D model for atmospheric passive transport and deposition of 
particles, aerosols, and radionuclides based on the so-called 
advection–diffusion–sedimentation (ADS) equation.
The model was originally developed for inert volcanic particles (tephra) and
has a track record of 50+ publications on different model applications and code 
validation, as well as an ever-growing community of users worldwide, including 
academia, private, research, and several institutions tasked with operational 
forecast of volcanic ash clouds. 

The code version 8.x has been redesigned and rewritten from
scratch in order to overcome legacy issues and allow for successive
optimisations in the preparation towards extreme-scale computing.
The new versions include significant improvements from the point of
view of model physics, numerical algorithmic methods, and computational
efficiency. In addition, the capabilities of the model have been extended
by incorporating new features such as the possibility of running ensemble
forecasts and dealing with multiple atmospheric species (i.e. volcanic ash
and gases, mineral dust, and radionuclides). Ensemble run capabilities are
supported since version 8.1, making it possible to quantify model uncertainties
and improve forecast quality.

The FALL3D code is one of the flagship codes included in the European
Centre of Excellence for Exascale in Solid Earth ([ChEESE](https://cheese2.eu)).
FALL3D is also available in [Zenodo](https://doi.org/10.5281/zenodo.6343786). 

## Documentation
For further information, please visit the FALL3D 
[User Guide](https://fall3d-suite.gitlab.io/fall3d/).

## Developers
FALL3D is developed and maintained by the **Consejo Superior de
Investigaciones Cientificas (CSIC)** and the **Istituto Nazionale di
Geofisica e Vulcanologia (INGV)**

1. [Arnau Folch](@afolch) 
2. [Antonio Costa](@ant.costa)
3. [Giovanni Macedonio](@gmacedonio)
4. [Leonardo Mingari](@lmingari)

## License and copyright
Copyright (C) 2013, 2016, 2017, 2018, 2019, 2021  Arnau Folch,
Antonio Costa and Giovanni Macedonio

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.

See the GNU General Public License 
[here](https://gitlab.com/fall3d-suite/fall3d/-/blob/master/LICENSE).
